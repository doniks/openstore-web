# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the openstore-web package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: openstore-web 1.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-08 23:33-0400\n"
"PO-Revision-Date: 2019-11-15 20:36+0000\n"
"Last-Translator: Bjørn curtis Knutson <b.knutson@tutanota.com>\n"
"Language-Team: Danish <https://translate.ubports.com/projects/openstore/"
"open-storeio/da/>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.8\n"

#: src/views/Package.vue:118
msgid ""
"*This app contains NSFW content, to view the description and\n"
"screenshots, click the above button."
msgstr ""
"Denne app indeholder NSFW indhold, for at se beskrivelse og\n"
"skærmbilleder, klik på knappen ovenover."

#: src/views/ManagePackage.vue:577
msgid "%{name} was successfully deleted"
msgstr "%[name] blev slettet uden fejl"

#: src/views/ManagePackage.vue:157
msgid "A comma separated list of keywords"
msgstr "En komma separeret liste af nøgleord"

#: src/App.vue:34 src/views/About.vue:3 src/views/About.vue:183
#: src/views/About.vue:187
msgid "About"
msgstr "Om"

#: src/App.vue:78
msgid "About Us"
msgstr "Om os"

#: src/views/Package.vue:287
msgid "Accounts"
msgstr "Konti"

#: src/views/Browse.vue:16
msgid "All Categories"
msgstr "Alle kategorier"

#: src/views/Browse.vue:31
msgid "All Types"
msgstr "Alle typer"

#: src/views/Login.vue:26
msgid "An account is not needed to download or install apps."
msgstr "En konto er ikke nødvendig for at hente eller installere apps."

#: src/views/ManagePackage.vue:444 src/views/ManagePackage.vue:462
#: src/views/ManageRevisions.vue:136
msgid "An error occured loading your app data"
msgstr "En fejl opstod under indlæsning af dine app data"

#: src/views/ManageRevisions.vue:183 src/views/Submit.vue:212
msgid "An unknown error has occured"
msgstr "En ukendt fejl er opstået"

#: src/views/About.vue:160
msgid "And many amazing developers, testers, and users from the OpenStore community!"
msgstr ""
"Og mange fantastiske udviklere, testere og brugere fra OpenStore "
"fællesskabet!"

#: src/views/About.vue:122
msgid ""
"Any personal data you decide to give us (e.g. your email address when registering for an account to upload your apps)\ngoes no further than us,\n"
"and will not be used for anything other than allowing you to maintain your apps and, in rare cases,\n"
"contact you in the event there is an issue with your app.\n"
"Additionally, this site uses cookies to keep you logged in to your account."
msgstr ""
"Personlig data du afgiver til os (f.eks. din email adresse når du registrere "
"dig til en konto for at uploade dine apps) kommer ikke længere end til os,\n"
"og vil ikke blive brugt til andet, end at tillade dig at vedligeholde dine "
"apps og i sjældne tilfælde,\n"
"kontakte dig i tilfælde af, at der er problemer med din app.\n"
"Yderligere, denne side bruger cookies for at holde dig logget ind på din "
"konto."

#: src/views/Manage.vue:14
msgid "API Key"
msgstr "API Nøgle"

#: src/components/Types.vue:3 src/views/Manage.vue:46 src/views/Stats.vue:108
msgid "App"
msgstr "App"

#: src/views/Submit.vue:95
msgid "App containing or promoting any of the above are strictly prohibited."
msgstr ""
"App der indeholder eller promoverer noget af ovenstående er strengt forbudt."

#: src/views/Submit.vue:133
msgid "App Name"
msgstr "App Navn"

#: src/App.vue:88
#, fuzzy
msgid "App Stats"
msgstr "App Stats"

#: src/views/Submit.vue:150
msgid "App Title"
msgstr "App Titel"

#: src/views/Stats.vue:19
msgid "App Type"
msgstr "App Type"

#: src/views/Stats.vue:15
msgid "App Types"
msgstr "App Typer"

#: src/views/Browse.vue:32
msgid "Apps"
msgstr "Apps"

#: src/views/Submit.vue:101
msgid ""
"Apps containing gambling involving real world money transactions are not allowed.\n"
"This includes, but is not limited to, online casinos, betting, and lotteries."
msgstr ""
"Apps indeholdende spil med rigtige penge transaktioner er ikke tilladt.\n"
"Dette inkludere, men er ikke begrænset til, online kasinoer, vædemål og "
"lotterier."

#: src/views/Submit.vue:82
msgid "Apps containing or promoting child abuse or child sexual abuse are strictly prohibited."
msgstr ""
"Apps der indeholder eller promoverer børnemishandling eller seksuelt misbrug "
"af børn er strengt forbudt."

#: src/views/Submit.vue:88
msgid ""
"Apps containing or promoting gratuitous violence are not allowed.\n"
"This includes, but is not limited to, violence, terrorism, bomb/weapon making, self harm, and grotesque imagery."
msgstr ""
"Apps der indeholder eller promoverer utilladelig vold er ikke tilladt.\n"
"Dette inkludere, men er ikke begrænset til, vold, terrorisme, bombe/våben "
"produktion, udførelse af selvskade og groteske billeder."

#: src/views/Submit.vue:75
#, fuzzy
msgid ""
"Apps containing or promoting sexually explicit content are not allowed.\n"
"This includes, but is not limited to, pornography and services promoting sexually explicit content."
msgstr ""
"Programmer der indeholder eller reklamere for sexuelt indhold , er ikke "
"tilladt .\n"
"dette inkludere , men er ikke begrænset til pornografi og services der "
"reklamere for sexuelle indhold"

#: src/views/Submit.vue:7
msgid "Apps not requiring manual review can be submitted by logging in with the \"Log In\" link above."
msgstr ""
"Apps der ikke kræver manuel genemsyn , kan indsendes , ved at logge ind med "
"\"Log ind\" ikonet oven for."

#: src/views/Submit.vue:108
#, fuzzy
msgid "Apps promoting illegal activities (based on USA law) are not allowed."
msgstr ""
"Apps der promoverer illegale aktiviteter (i.h.t. Amerikansk lovgivning) er "
"ikke tilladt."

#: src/views/Submit.vue:18
msgid "Apps requiring manual review must be submitted by contacting an admin in our telegram group."
msgstr ""
"Apps der behøver manuel gennemgang , skal indsendes ved at kontakte en admin "
", igennem vores telegram gruppe."

#: src/views/Submit.vue:114
msgid ""
"Apps that are found to be stealing the users data, trying to escalate their privileges without user consent,\n"
"or executing malicious process are strictly prohibited."
msgstr ""

#: src/views/About.vue:129
msgid ""
"Apps uploaded to the OpenStore are subject to our submission rules and content policy.\n"
"You can review the rules and policies before uploading your app."
msgstr ""

#: src/views/ManagePackage.vue:258 src/views/Package.vue:218
msgid "Architecture"
msgstr "Arkitektur"

#: src/views/Package.vue:288
msgid "Audio"
msgstr "Lyd"

#: src/views/Manage.vue:47 src/views/ManagePackage.vue:254
#: src/views/Package.vue:180
msgid "Author"
msgstr "Forfatter"

#: src/views/Package.vue:7
msgid "Back"
msgstr "Tilbage"

#: src/views/Badge.vue:28 src/views/Badge.vue:32 src/views/ManagePackage.vue:70
msgid "Badge"
msgstr ""

#: src/views/About.vue:18
msgid "Badges"
msgstr ""

#: src/views/Package.vue:289
msgid "Bluetooth"
msgstr "Bluetooth"

#: src/App.vue:31
#, fuzzy
msgid "Browse"
msgstr "Browse"

#: src/views/Browse.vue:162
#, fuzzy
msgid "Browse Apps"
msgstr "Browse Apps"

#: src/views/About.vue:74
msgid "Bug Tracker"
msgstr "Fejl søger"

#: src/views/Package.vue:290
msgid "Calendar"
msgstr "Kalender"

#: src/views/Package.vue:291
msgid "Camera"
msgstr "Kamera"

#: src/views/ManagePackage.vue:358 src/views/ManageRevisions.vue:66
#: src/views/Submit.vue:167
msgid "Cancel"
msgstr "Annuller"

#: src/views/Browse.vue:14 src/views/ManagePackage.vue:140
#: src/views/Package.vue:202
msgid "Category"
msgstr "Kategori"

#: src/views/ManagePackage.vue:101 src/views/ManageRevisions.vue:50
#: src/views/Package.vue:128
msgid "Changelog"
msgstr "Ændringslog"

#: src/views/ManagePackage.vue:295 src/views/ManageRevisions.vue:13
msgid "Channel"
msgstr "Kanal"

#: src/views/ManagePackage.vue:274
msgid "Channels"
msgstr "Kanaler"

#: src/App.vue:83
msgid "Chat with us on Telegram"
msgstr "Chat med os på Telegram"

#: src/views/Submit.vue:81
msgid "Child Endangerment"
msgstr ""

#: src/views/ManagePackage.vue:142
msgid "Choose a category"
msgstr "Vælg kategori"

#: src/views/ManagePackage.vue:173
msgid "Choose a license"
msgstr "Vælg en licens"

#: src/views/ManagePackage.vue:231
msgid "Choose a maintainer"
msgstr "Vælg den der vedligeholder"

#: src/views/Package.vue:292
msgid "Connectivity"
msgstr ""

#: src/views/About.vue:59
msgid "Contact"
msgstr ""

#: src/views/Package.vue:293
msgid "Contacts"
msgstr ""

#: src/views/Package.vue:295
msgid "Content Exchange"
msgstr ""

#: src/views/Package.vue:294
msgid "Content Exchange Source"
msgstr ""

#: src/views/About.vue:138
msgid "Contributors"
msgstr ""

#: src/components/CopyLine.vue:9
msgid "Copy to clipboard"
msgstr ""

#: src/views/Stats.vue:20
msgid "Count"
msgstr ""

#: src/views/ManageRevisions.vue:57
msgid "Create"
msgstr ""

#: src/views/Package.vue:296
msgid "Debug"
msgstr ""

#: src/views/ManagePackage.vue:353
msgid "Delete"
msgstr ""

#: src/views/ManagePackage.vue:90
msgid "Description"
msgstr ""

#: src/views/ManagePackage.vue:55
msgid "Discovery"
msgstr ""

#: src/views/Package.vue:91
msgid "Donate"
msgstr ""

#: src/views/ManagePackage.vue:216
msgid "Donate URL"
msgstr ""

#: src/views/Package.vue:283
msgid "Download"
msgstr ""

#: src/views/ManagePackage.vue:288
msgid "Download Stats"
msgstr ""

#: src/views/About.vue:43
msgid "Download the OpenStore App"
msgstr ""

#: src/views/Manage.vue:50 src/views/Manage.vue:70
#: src/views/ManagePackage.vue:297
msgid "Downloads"
msgstr ""

#: src/views/Package.vue:166
msgid "Downloads of the latest version"
msgstr ""

#: src/views/ManagePackage.vue:119
msgid "Drag and drop to sort screenshots."
msgstr ""

#: src/views/ManagePackage.vue:8
msgid "Edit"
msgstr ""

#: src/views/ManagePackage.vue:443 src/views/ManagePackage.vue:461
#: src/views/ManagePackage.vue:562 src/views/ManagePackage.vue:590
#: src/views/ManageRevisions.vue:135 src/views/Submit.vue:218
msgid "Error"
msgstr ""

#: src/views/Feeds.vue:34 src/views/Feeds.vue:38
msgid "Feeds"
msgstr ""

#: src/views/ManagePackage.vue:266
msgid "File Size"
msgstr ""

#: src/views/ManageRevisions.vue:22
msgid "File Upload"
msgstr ""

#: src/views/About.vue:61
msgid ""
"For any questions you may have or help you may need just chat with us in our telegram group.\n"
"You can also submit issues, bugs, and feature requests to our GitLab bug tracker."
msgstr ""

#: src/App.vue:93
msgid "Fork us on GitLab"
msgstr ""

#: src/views/ManagePackage.vue:262 src/views/Package.vue:214
msgid "Framework"
msgstr ""

#: src/views/Package.vue:314
msgid "Full System Access"
msgstr ""

#: src/views/Submit.vue:100
msgid "Gambling"
msgstr ""

#: src/views/About.vue:25
msgid "Get your badge"
msgstr ""

#: src/views/Submit.vue:39
msgid ""
"Give a short explanation why you can’t publish the app without extra privileges.\n"
"No need to go into details, a one liner like \"needs to run a daemon in the background\" will do.\n"
"List all the special features you have, if there are more."
msgstr ""

#: src/views/Browse.vue:187 src/views/Manage.vue:174
msgid "Go back a page"
msgstr ""

#: src/views/Browse.vue:188 src/views/Manage.vue:175
msgid "Go to the next page"
msgstr ""

#: src/views/About.vue:86
msgid "GPL v3 License"
msgstr ""

#: src/views/Submit.vue:94
msgid "Harassment, Bullying, or Hate Speech"
msgstr ""

#: src/views/Package.vue:297
msgid "History"
msgstr ""

#: src/views/Manage.vue:44
msgid "Icon"
msgstr ""

#: src/views/ManagePackage.vue:250
msgid "ID"
msgstr ""

#: src/views/Submit.vue:44
msgid ""
"If an application could be published without manual review if it wouldn’t be for that one cool feature,\npublish a stripped down version!\n"
"Not everyone will want to install apps with less confinement."
msgstr ""

#: src/views/Submit.vue:107
msgid "Illegal Activities"
msgstr ""

#: src/views/Package.vue:298
msgid "In App Purchases"
msgstr ""

#: src/views/ManagePackage.vue:60 src/views/Package.vue:177
msgid "Info"
msgstr ""

#: src/views/About.vue:29 src/views/Package.vue:39
msgid "Install"
msgstr ""

#: src/views/Package.vue:285
msgid "Install via the OpenStore app"
msgstr ""

#: src/views/Browse.vue:186 src/views/Manage.vue:173
msgid "Jump to the first page"
msgstr ""

#: src/views/Browse.vue:189 src/views/Manage.vue:176
msgid "Jump to the last page"
msgstr ""

#: src/views/Package.vue:299
msgid "Keep Display On"
msgstr ""

#: src/views/Manage.vue:18
msgid "Keep your api key private!"
msgstr ""

#: src/views/ManagePackage.vue:155
msgid "Keywords"
msgstr ""

#: src/App.vue:60 src/components/BadgeSelect.vue:4
msgid "Language"
msgstr ""

#: src/views/Browse.vue:46
msgid "Latest Update"
msgstr ""

#: src/views/About.vue:78 src/views/ManagePackage.vue:171
#: src/views/Package.vue:198
msgid "License"
msgstr ""

#: src/views/Package.vue:300
msgid "Location"
msgstr ""

#: src/App.vue:37
msgid "Log In"
msgstr ""

#: src/views/Login.vue:24
msgid "Log in to the OpenStore to be able to manage your apps."
msgstr ""

#: src/App.vue:43
msgid "Log Out"
msgstr ""

#: src/views/Login.vue:40 src/views/Login.vue:44
msgid "Login"
msgstr ""

#: src/views/Submit.vue:125
msgid "Login to submit your app"
msgstr ""

#: src/views/Login.vue:7
msgid "Login via GitHub"
msgstr ""

#: src/views/Login.vue:12
msgid "Login via GitLab"
msgstr ""

#: src/views/Login.vue:18
msgid "Login via Ubuntu"
msgstr ""

#: src/views/ManagePackage.vue:229
msgid "Maintainer"
msgstr ""

#: src/views/Submit.vue:113
msgid "Malware"
msgstr ""

#: src/App.vue:40 src/views/Manage.vue:152 src/views/ManagePackage.vue:381
#: src/views/ManagePackage.vue:383
msgid "Manage"
msgstr ""

#: src/views/Manage.vue:148
msgid "Manage Apps"
msgstr ""

#: src/views/Package.vue:301
msgid "Microphone"
msgstr ""

#: src/views/Package.vue:303
msgid "Music Files"
msgstr ""

#: src/views/Manage.vue:45
msgid "Name"
msgstr ""

#: src/views/About.vue:48
msgid "Navigate to the downloads folder"
msgstr ""

#: src/views/Package.vue:304
msgid "Networking"
msgstr ""

#: src/views/Feeds.vue:9
msgid "New Apps"
msgstr ""

#: src/views/ManagePackage.vue:37 src/views/ManageRevisions.vue:86
#: src/views/ManageRevisions.vue:88
msgid "New Revision"
msgstr ""

#: src/views/ManageRevisions.vue:178
msgid "New revision for %{channel} was created!"
msgstr ""

#: src/views/ManageRevisions.vue:8
msgid "New Revision for %{name}"
msgstr ""

#: src/views/Browse.vue:44
msgid "Newest"
msgstr ""

#: src/views/ManagePackage.vue:31
msgid "No"
msgstr ""

#: src/views/Browse.vue:101 src/views/Manage.vue:94
msgid "No apps found."
msgstr ""

#: src/views/Manage.vue:81 src/views/Manage.vue:86 src/views/Package.vue:156
msgid "None"
msgstr ""

#: src/views/Manage.vue:75
msgid "Not published"
msgstr ""

#: src/views/ManagePackage.vue:161
msgid "NSFW"
msgstr ""

#: src/views/Browse.vue:90
msgid "NSFW content"
msgstr ""

#: src/views/Browse.vue:45
msgid "Oldest"
msgstr ""

#: src/views/Browse.vue:47
msgid "Oldest Update"
msgstr ""

#: src/views/Submit.vue:60
msgid ""
"One of the main goals of the OpenStore is to provide a safe app store for people of all ages.\n"
"To accomplish this goal we do not allow certain types of app to be published.\n"
"Failure to follow the guidelines will result in your app being removed.\n"
"Any account with serious infractions may be subject to termination.\n"
"The OpenStore team thanks you for helping us promote a better enviroment for everyone."
msgstr ""

#: src/views/Submit.vue:48
msgid ""
"Only open source applications allowed for manual review:\n"
"As the applications might have arbitrary access to the device, every manually reviewed app will get a source code review."
msgstr ""

#: src/views/ManagePackage.vue:223
msgid "Only YouTube videos are supported at this time. Make sure the url is for the embedded video!"
msgstr ""

#: src/views/About.vue:47
msgid "Open the terminal app"
msgstr ""

#: src/views/About.vue:145
msgid "OpenStore API/Server Contributors"
msgstr ""

#: src/views/Submit.vue:58
msgid "OpenStore App Content Policy"
msgstr ""

#: src/views/About.vue:155
msgid "OpenStore App Contributors"
msgstr ""

#: src/views/Badge.vue:3
msgid "OpenStore Badges"
msgstr ""

#: src/views/About.vue:67
msgid "OpenStore group on Telegram"
msgstr ""

#: src/views/About.vue:93
msgid "OpenStore source on GitLab"
msgstr ""

#: src/views/About.vue:150
msgid "OpenStore Website Contributors"
msgstr ""

#: src/views/ManageRevisions.vue:33
msgid "OR"
msgstr ""

#: src/views/ManagePackage.vue:245
msgid "Package Info"
msgstr ""

#: src/views/Package.vue:187
msgid "Packager/Publisher"
msgstr ""

#: src/views/NotFound.vue:3
msgid "Page Not Found"
msgstr ""

#: src/views/Package.vue:80 src/views/Package.vue:135
msgid "Permissions"
msgstr ""

#: src/views/Package.vue:306
msgid "Picture Files"
msgstr ""

#: src/views/ManagePackage.vue:43
msgid "Presentation"
msgstr ""

#: src/views/Submit.vue:69
msgid "Prohibited Apps"
msgstr ""

#: src/views/About.vue:20
msgid "Promote your app on the OpenStore!"
msgstr ""

#: src/views/Badge.vue:5
msgid ""
"Promote your app on the OpenStore! Add these badges to your website,\n"
"code repository, social media, or anywhere on the internet. The badges\n"
"are available in several different languages and in both PNG and SVG\n"
"formats."
msgstr ""

#: src/views/ManagePackage.vue:14
msgid "Public Link"
msgstr ""

#: src/views/ManagePackage.vue:21
msgid "Publish"
msgstr ""

#: src/views/Manage.vue:74 src/views/Package.vue:210
msgid "Published"
msgstr ""

#: src/views/ManagePackage.vue:323
msgid "Published Date"
msgstr ""

#: src/views/Package.vue:307
msgid "Push Notifications"
msgstr ""

#: src/views/Package.vue:302
msgid "Read Music Files"
msgstr ""

#: src/views/Package.vue:305
msgid "Read Picture Files"
msgstr ""

#: src/views/Package.vue:310
msgid "Read Video Files"
msgstr ""

#: src/views/Browse.vue:41
msgid "Relevance"
msgstr ""

#: src/App.vue:103
msgid "Report an Issue"
msgstr ""

#: src/views/Package.vue:397
msgid "Restricted permission"
msgstr ""

#: src/views/ManagePackage.vue:294
msgid "Revision"
msgstr ""

#: src/App.vue:108 src/views/Feeds.vue:3
msgid "RSS Feeds"
msgstr ""

#: src/views/Submit.vue:4
msgid "Rules for Submission"
msgstr ""

#: src/views/Submit.vue:31
msgid "Rules for Submissions Requiring Manual Review"
msgstr ""

#: src/views/About.vue:50
msgid "Run the command:"
msgstr ""

#: src/views/ManagePackage.vue:348
msgid "Save"
msgstr ""

#: src/components/Types.vue:4 src/views/Stats.vue:109
msgid "Scope"
msgstr ""

#: src/views/Browse.vue:34
msgid "Scopes"
msgstr ""

#: src/views/Package.vue:107
msgid "Screenshots"
msgstr ""

#: src/views/ManagePackage.vue:106
msgid "Screenshots (Limit 5)"
msgstr ""

#: src/views/Browse.vue:9 src/views/Manage.vue:26
msgid "Search"
msgstr ""

#: src/views/Submit.vue:35
msgid "Send us a link to a repository for your app to our telegram group along with some instructions on how to build it."
msgstr ""

#: src/views/Package.vue:308
msgid "Sensors"
msgstr ""

#: src/views/Submit.vue:74
msgid "Sexually Explicit Content"
msgstr ""

#: src/views/Package.vue:117
msgid "Show NSFW Content"
msgstr ""

#: src/App.vue:98
msgid "Site Status"
msgstr ""

#: src/views/Browse.vue:39
msgid "Sort By"
msgstr ""

#: src/views/Package.vue:194
msgid "Source"
msgstr ""

#: src/views/ManagePackage.vue:206
msgid "Source URL"
msgstr ""

#: src/views/ManagePackage.vue:65 src/views/Package.vue:162
#: src/views/Stats.vue:3 src/views/Stats.vue:92 src/views/Stats.vue:96
msgid "Stats"
msgstr ""

#: src/views/Manage.vue:48
msgid "Status"
msgstr ""

#: src/App.vue:28 src/views/Submit.vue:162
msgid "Submit"
msgstr ""

#: src/views/Manage.vue:10 src/views/Submit.vue:129 src/views/Submit.vue:189
#: src/views/Submit.vue:193
msgid "Submit App"
msgstr ""

#: src/views/About.vue:134
msgid "Submit your app"
msgstr ""

#: src/views/ManagePackage.vue:551 src/views/ManagePackage.vue:576
#: src/views/ManageRevisions.vue:177
msgid "Success"
msgstr ""

#: src/views/Package.vue:190
msgid "Support"
msgstr ""

#: src/views/ManagePackage.vue:211
msgid "Support URL"
msgstr ""

#: src/views/ManagePackage.vue:85
msgid "Tag Line"
msgstr ""

#: src/views/About.vue:102
msgid "Terms"
msgstr ""

#: src/views/Package.vue:13
msgid "The app you are looking for has been removed or does not exist"
msgstr ""

#: src/views/About.vue:97
msgid ""
"The apps available for download from the OpenStore have their own licencing and terms.\n"
"Consult each individual app's page for more information."
msgstr ""

#: src/views/ManagePackage.vue:552
msgid "The changes to %{name} were saved!"
msgstr ""

#: src/views/About.vue:104
msgid ""
"The OpenStore a non-profit volunteer project.\n"
"Although every effort is made to ensure that everything in the repository is safe to install, you use it AT YOUR OWN RISK.\n"
"Apps uploaded to the OpenStore will be subject to an automated review process or\n"
"a manual review process to check for potential security or privacy issues.\n"
"This checking is far from exhaustive though, and there are no guarantees."
msgstr ""

#: src/views/About.vue:31
msgid ""
"The OpenStore app is installed by default on the UBports builds of Ubuntu Touch.\n"
"But if you need to install the OpenStore manually, follow these steps:"
msgstr ""

#: src/views/About.vue:141
msgid "The OpenStore is made possible by many amazing contributors:"
msgstr ""

#: src/views/About.vue:5
msgid ""
"The OpenStore is the official app store for Ubuntu Touch.\n"
"It is an open source project run by a team of volunteers with help from the community.\n"
"You can discover and install new apps on your Ubuntu Touch device.\n"
"You can also upload and manage your own apps for publication.\n"
"The OpenStore encourages the apps published within to be open source, but also accepts proprietary apps."
msgstr ""

#: src/views/About.vue:112
msgid ""
"The OpenStore respects your privacy.\n"
"We don't track you or your device.\n"
"You do not need an account to download and install apps and we do not track your downloads.\n"
"For developer information we count the number of downloads per app revision.\n"
"Information about your device (the cpu architecture, framework versions, OS version, and OS language)\n"
"are sent to the server to allow it to filter apps to only what you can install and present your native language where possible.\n"
"This information is not logged or stored and cannot be used to uniquely identify you."
msgstr ""

#: src/views/NotFound.vue:5
msgid "The page you are looking for has been removed or does not exist"
msgstr ""

#: src/views/About.vue:80
msgid ""
"The source code for OpenStore related projects is available under the GPL v3.\n"
"You can find the code on the OpenStore GitLab page."
msgstr ""

#: src/views/Package.vue:17
msgid "There was an error trying to find this app, please refresh and try again."
msgstr ""

#: src/views/Browse.vue:65 src/views/Manage.vue:31
msgid "There was an error trying to load the app list, please refresh and try again."
msgstr ""

#: src/views/Stats.vue:5
msgid "There was an error trying to load the stats, please refresh and try again."
msgstr ""

#: src/views/ManagePackage.vue:163
msgid "This app contains NSFW material"
msgstr ""

#: src/views/Package.vue:409
msgid "This app does not have access to restricted system data, see permissions for more details"
msgstr ""

#: src/views/Package.vue:411
msgid "This app has access to restricted system data, see permissions for more details"
msgstr ""

#: src/views/Submit.vue:141
msgid ""
"This is the unique identifier for your app.\n"
"It must match exactly the \"name\" field in your click's manifest.json and must be all lowercase letters.\n"
"For example: \"openstore.openstore-team\", where \"openstore\" is the app and \"openstore-team\"\n"
"is the group or individual authoring the app."
msgstr ""

#: src/views/ManageRevisions.vue:52
msgid "This will be added to the beginning of your current changelog"
msgstr ""

#: src/views/ManagePackage.vue:80
msgid "Title"
msgstr ""

#: src/views/Browse.vue:42
msgid "Title (A-Z)"
msgstr ""

#: src/views/Browse.vue:43
msgid "Title (Z-A)"
msgstr ""

#: src/views/ManagePackage.vue:314
msgid "Total"
msgstr ""

#: src/views/Package.vue:170
msgid "Total downloads"
msgstr ""

#: src/views/Manage.vue:79
msgid "Total:"
msgstr ""

#: src/views/ManagePackage.vue:279 src/views/Package.vue:222
msgid "Translation Languages"
msgstr ""

#: src/views/Browse.vue:102 src/views/Manage.vue:95
msgid "Try searching for something else"
msgstr ""

#: src/views/Browse.vue:29 src/views/ManagePackage.vue:270
msgid "Type"
msgstr ""

#: src/views/About.vue:14
msgid "Ubuntu Touch"
msgstr ""

#: src/views/Browse.vue:3
msgid "Ubuntu Touch Apps"
msgstr ""

#: src/views/Stats.vue:106
msgid "Uncategorized"
msgstr ""

#: src/views/Package.vue:149
msgid "Unrestricted read access to:"
msgstr ""

#: src/views/Package.vue:145
msgid "Unrestricted write access to:"
msgstr ""

#: src/views/Package.vue:206
msgid "Updated"
msgstr ""

#: src/views/Feeds.vue:18
msgid "Updated Apps"
msgstr ""

#: src/views/ManagePackage.vue:331
msgid "Updated Date"
msgstr ""

#: src/views/ManageRevisions.vue:36
msgid "URL"
msgstr ""

#: src/views/ManageRevisions.vue:44
msgid "URL of App from the Web"
msgstr ""

#: src/views/Package.vue:309
msgid "User Metrics"
msgstr ""

#: src/views/Manage.vue:49 src/views/ManagePackage.vue:296
msgid "Version"
msgstr ""

#: src/views/Package.vue:312
msgid "Video"
msgstr ""

#: src/views/Package.vue:311
msgid "Video Files"
msgstr ""

#: src/views/ManagePackage.vue:221
msgid "Video URL"
msgstr ""

#: src/views/About.vue:37
msgid "View Install Instructions"
msgstr ""

#: src/views/Submit.vue:87
msgid "Violence"
msgstr ""

#: src/components/Types.vue:5 src/views/Stats.vue:110
msgid "Web App"
msgstr ""

#: src/components/Types.vue:6 src/views/Stats.vue:111
msgid "Web App+"
msgstr ""

#: src/views/Browse.vue:33
msgid "Web Apps"
msgstr ""

#: src/views/Package.vue:313
msgid "Webview"
msgstr ""

#: src/views/Manage.vue:4
msgid "Welcome %{name}!"
msgstr ""

#: src/views/ManageRevisions.vue:15 src/views/ManageRevisions.vue:174
msgid "Xenial"
msgstr ""

#: src/views/ManagePackage.vue:30
msgid "Yes"
msgstr ""

#: src/views/Submit.vue:10
msgid "You are only allowed to publish apps that you have permission to distribute."
msgstr ""

#: src/views/Submit.vue:22
msgid "You must read over the content policy detailed below."
msgstr ""

#: src/views/Submit.vue:13
msgid ""
"Your app can be pulled without warning at the discretion of our admins.\n"
"Where possible, we will contact you regarding any such actions."
msgstr ""
